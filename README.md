To build , clone this repo and in this folder run:

**mvn clean install**

to run:

**mvn spring-boot:run**


Postman with the example to the rest api url's and data examples.

[Postman collection](https://documenter.getpostman.com/view/4358522/RznJncSe)