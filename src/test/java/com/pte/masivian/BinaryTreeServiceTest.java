/**
 * 
 */
package com.pte.masivian;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.pte.masivian.data.Data;
import com.pte.masivian.data.Node;
import com.pte.masivian.services.impl.BinaryTreeServiceImpl;

/**
 * @author wavaleros
 *
 */
@RunWith(SpringRunner.class)

public class BinaryTreeServiceTest {
	@TestConfiguration
	static class BinaryTreeServiceTestConfiguration {
		@Bean
		public BinaryTreeServiceImpl binaryTreeServiceImpl() {
			return new BinaryTreeServiceImpl();
		}

		@Bean
		public Data data() {
			return new Data();
		}
	}

	@Autowired
	BinaryTreeServiceImpl binaryTreeServiceImpl;
	@Autowired
	Data data;

	@Test
	/**
	 * Test for the insertion in the binary tree
	 */
	public void addTesting() {
		binaryTreeServiceImpl.add(null, 70);
		Node root = data.getData().get(70).getRoot();
		binaryTreeServiceImpl.add(root, 71);
		binaryTreeServiceImpl.add(root, 72);
		assertTrue(data.getData().get(70).getRoot().getRight().getRight().getValue() == 72);
		binaryTreeServiceImpl.add(root, 60);
		binaryTreeServiceImpl.add(root, 40);
		assertTrue(data.getData().get(70).getRoot().getLeft().getLeft().getValue() == 40);
		binaryTreeServiceImpl.add(root, 69);
		assertTrue(data.getData().get(70).getRoot().getLeft().getRight().getValue() == 69);

	}

	@Test
	/**
	 * Test for search ultra-left , ultra-right and left-right value.
	 */
	public void searchPath() {
		basicTree();
		Node root = data.getData().get(70).getRoot();
		List<Integer> path = new ArrayList<>();
		assertTrue(binaryTreeServiceImpl.searchPath(path, root, 39));
		// Verify the father value (last entry in path)
		assertTrue(40 == path.get(path.size() - 1));
		path.clear();
		assertTrue(binaryTreeServiceImpl.searchPath(path, root, 61));
		// Verify the father value (last entry in path)
		assertTrue(60 == path.get(path.size() - 1));
		path.clear();
		assertTrue(binaryTreeServiceImpl.searchPath(path, root, 72));
		// Verify the father value (last entry in path)
		assertTrue(71 == path.get(path.size() - 1));
	}

	@Test
	public void acenstor() {
		basicTree();
		int actual = binaryTreeServiceImpl.ancestor(70, 39, 41);
		assertEquals(40, actual);
		actual = binaryTreeServiceImpl.ancestor(70, 39, 61);
		assertEquals(50, actual);
		actual = binaryTreeServiceImpl.ancestor(70, 39, 72);
		assertEquals(70, actual);
	}

	/**
	 * create a simple tree for testing purpose 70 50 71 40 60 72 39 41 61
	 */
	private void basicTree() {
		data.getData().clear();
		binaryTreeServiceImpl.add(null, 70);
		Node root = data.getData().get(70).getRoot();
		binaryTreeServiceImpl.add(root, 50);
		binaryTreeServiceImpl.add(root, 71);
		binaryTreeServiceImpl.add(root, 72);
		binaryTreeServiceImpl.add(root, 50);
		binaryTreeServiceImpl.add(root, 40);
		binaryTreeServiceImpl.add(root, 60);
		binaryTreeServiceImpl.add(root, 60);
		binaryTreeServiceImpl.add(root, 61);
		binaryTreeServiceImpl.add(root, 39);
		binaryTreeServiceImpl.add(root, 41);
	}

}
