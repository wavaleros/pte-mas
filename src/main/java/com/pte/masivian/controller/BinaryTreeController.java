/**
 * 
 */
package com.pte.masivian.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pte.masivian.data.Data;
import com.pte.masivian.data.Node;
import com.pte.masivian.services.impl.BinaryTreeServiceImpl;

/**
 * @author wavaleros
 *
 */
@RestController
@RequestMapping("/tree")
public class BinaryTreeController {
	@Autowired
	BinaryTreeServiceImpl binaryTreeServiceImpl;
	@Autowired
	Data data;

	/**
	 * Method to create a tree given the root and the subsequent values.
	 * 
	 * @param root
	 * @param values
	 * @return
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody String values) {
		String[] valueList = values.split(" ");
		int intRoot = Integer.parseInt(valueList[0]);
		binaryTreeServiceImpl.add(null, intRoot);

		Node node = data.getData().get(intRoot).getRoot();
		for (int i = 1; i < valueList.length; i++) {
			binaryTreeServiceImpl.add(node, Integer.parseInt(valueList[i]));
		}
		return ResponseEntity.ok(data.getData().get(intRoot));
	}

	@RequestMapping(value = "/{root}/node/add/{value}", method = RequestMethod.PUT)
	public ResponseEntity<?> addNode(@PathVariable int root, @PathVariable int value) {
		Node nodeRoot = data.getData().get(root).getRoot();
		binaryTreeServiceImpl.add(nodeRoot, value);
		return ResponseEntity.ok(data.getData().get(root));
	}

	@RequestMapping(value = "/{root}/ancestor/{left}/{right}", method = RequestMethod.GET)
	public ResponseEntity<?> getAncestor(@PathVariable int root, @PathVariable int left, @PathVariable int right) {
		int ancestor = binaryTreeServiceImpl.ancestor(root, left, right);
		return ResponseEntity.ok(ancestor);
	}

}
