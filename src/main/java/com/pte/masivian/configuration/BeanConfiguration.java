/**
 * 
 */
package com.pte.masivian.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.pte.masivian.data.Data;

/**
 * @author wavaleros
 *
 */
@Component
public class BeanConfiguration {

	@Bean
	public Data data() {
		return new Data();
	}

}
