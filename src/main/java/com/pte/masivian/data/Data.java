/**
 * 
 */
package com.pte.masivian.data;

import java.util.HashMap;

/**
 * @author wavaleros
 *
 */
public class Data {
	private HashMap<Integer, BinaryTree> data;

	public Data() {
		data = new HashMap<Integer, BinaryTree>();
	}

	public HashMap<Integer, BinaryTree> getData() {
		return data;
	}

	public void setData(HashMap<Integer, BinaryTree> data) {
		this.data = data;
	}

}
