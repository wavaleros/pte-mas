/**
 * 
 */
package com.pte.masivian.data;

/**
 * @author wavaleros
 *
 */
public class BinaryTree {
	private Node root;

	public BinaryTree(Node node) {
		this.setRoot(node);
	}

	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) {
		this.root = root;
	}
}
