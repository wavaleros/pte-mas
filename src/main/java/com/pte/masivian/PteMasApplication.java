package com.pte.masivian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.pte.masivian.controller.BinaryTreeController;

@SpringBootApplication
@ComponentScan(basePackages = { "com.pte.masivian.controller", "com.pte.masivian.configuration",
		"com.pte.masivian.services.impl" })
public class PteMasApplication {

	public static void main(String[] args) {
		SpringApplication.run(PteMasApplication.class, args);
	}

}
