/**
 * 
 */
package com.pte.masivian.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pte.masivian.data.BinaryTree;
import com.pte.masivian.data.Data;
import com.pte.masivian.data.Node;
import com.pte.masivian.services.BinaryTreeService;

/**
 * @author wavaleros
 *
 */
@Service
public class BinaryTreeServiceImpl implements BinaryTreeService {
	@Autowired
	Data data;

	@Override
	public void add(Node root, int value) {

		if (root != null) {
			// Add node to existing tree
			addRecursive(root, value);

		} else {
			// Add new root node
			BinaryTree bt = new BinaryTree(addRecursive(null, value));
			data.getData().put(value, bt);
		}

	}

	private Node addRecursive(Node current, int value) {
		if (current == null) {
			Node n = new Node(value);
			return n;
		}
		if (value < current.getValue()) {
			current.setLeft(addRecursive(current.getLeft(), value));
		} else if (value > current.getValue()) {
			current.setRight(addRecursive(current.getRight(), value));
		}
		return current;

	}

	@Override
	public int ancestor(int tree, int left, int right) {
		int ancestor = -1;
		Node root = data.getData().get(tree).getRoot();
		// find the path for the left,
		List<Integer> pathLeft = new ArrayList<Integer>();
		List<Integer> pathRight = new ArrayList<Integer>();
		boolean leftFound = searchPath(pathLeft, root, left);
		boolean rightFound = searchPath(pathRight, root, right);
		if (leftFound && rightFound) {
			ancestor = pathComparison(pathLeft, pathRight);
		}

		return ancestor;
	}

	private int pathComparison(List<Integer> pathLeft, List<Integer> pathRight) {
		int ancestor = -1;
		List<Integer> shortest = new ArrayList<Integer>();
		List<Integer> largest = new ArrayList<Integer>();
		if (pathLeft.size() < pathRight.size()) {
			shortest = pathLeft;
			largest = pathRight;
		} else {
			shortest = pathRight;
			largest = pathLeft;
		}
		int i = 0;
		while (i < shortest.size() && shortest.get(i) == largest.get(i)) {
			ancestor = shortest.get(i);
			i++;
		}
		return ancestor;
	}

	@Override
	public String print(Node node) {
		if (node == null) {
			return "";
		}
		return node.getValue() + " " + print(node.getLeft()) + " " + print(node.getRight());

	}

	@Override
	public boolean searchPath(List<Integer> path, Node root, int value) {
		boolean response = false;
		Node current = root;
		while (current.getLeft() != null || current.getRight() != null) {

			if (value < current.getValue()) {
				path.add(current.getValue());
				current = current.getLeft();
			}
			if (value > current.getValue()) {
				path.add(current.getValue());
				current = current.getRight();
			}
		}
		if (current.getValue() == value) {
			response = true;
		}
		return response;
	}

}
