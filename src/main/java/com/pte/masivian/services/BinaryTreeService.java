/**
 * 
 */
package com.pte.masivian.services;

import java.util.List;

import com.pte.masivian.data.Node;

/**
 * @author wavaleros
 *
 */
public interface BinaryTreeService {

	public void add(Node root, int value);

	public int ancestor(int tree, int left, int right);

	public String print(Node root);

	public boolean searchPath(List<Integer> path, Node root, int value);

}
